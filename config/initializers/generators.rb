# frozen_string_literal: true
# Config default Rails generators
Rails.application.config.generators do |g|
  g.orm             :active_record
  g.template_engine :erb
  g.test_framework  :test_unit
  g.stylesheets     false
  g.javascripts     false
  g.helper          false
end
