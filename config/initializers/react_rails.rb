# frozen_string_literal: true
# Settings for the pool of renderers:
Rails.application.config.react.server_renderer_pool_size  ||= 1  # ExecJS doesn't allow more than one on MRI
Rails.application.config.react.server_renderer_timeout    ||= 20 # seconds
Rails.application.config.react.server_renderer = React::ServerRendering::SprocketsRenderer
Rails.application.config.react.server_renderer_options = {
  files: %w(server_prerender.js), # files to load for prerendering
  replay_console: true, # if true, console.* will be replayed client-side
}
