# frozen_string_literal: true
Rails.application.routes.draw do
  get '/dang_nhap' => 'sessions#new', as: :sign_in

  get '/dang_ky' => 'registrations#new', as: :sign_up
  get '/dang_ky/:account_type' => 'registrations#new'

  get '/gui_yeu_cau' => 'jobs#new', as: :new_request
  resources :jobs, only: [:index, :new, :edit]

  get 'home/index'

  root 'home#index'
end
