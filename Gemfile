# frozen_string_literal: true
source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Use dotenv to read environment variables from .env file
gem 'dotenv-rails'

# Use ActiveResource to implement object-relational mapping for REST web services
gem 'activeresource', github: 'rails/activeresource', branch: 'master'
# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.1'
gem 'rails-i18n', github: 'svenfuchs/rails-i18n', branch: 'master'
# A set of responders modules to dry up app.
gem 'responders'
# Use postgresql as the database for Active Record
gem 'pg', '~> 0.18'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use React as front-end framework
gem 'react-rails'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Semantic UI SASS version
# gem 'semantic-ui-sass', github: 'doabit/semantic-ui-sass', branch: 'master'
# Use Semantic UI LESS version
gem 'autoprefixer-rails'
gem 'less-rails-semantic_ui'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 3.0'
# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platform: :mri
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'web-console', '>= 3.3.0'

  gem 'overcommit', require: false
  gem 'rubocop', '~> 0.46.0', require: false
  gem 'rubycritic', require: false

  gem 'guard'
  gem 'guard-minitest'
  gem 'guard-rubocop'

  # Use Capistrano for deployment
  gem 'capistrano', '~> 3.6'
  gem 'capistrano-bundler'
  gem 'capistrano-passenger'
  gem 'capistrano-rails', '~> 1.2'
  gem 'capistrano-rvm'
end

group :test do
  gem 'minitest-reporters', require: false
  gem 'rack-test', require: 'rack/test'
  gem 'ruby-prof'
  gem 'simplecov', require: false
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
