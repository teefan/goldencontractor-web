class NewJobStoreClass extends FluxUtils.ReduceStore {
  constructor() {
    super(AppDispatcher);
  }

  getInitialState() {
    return {
      jobFormData: {
        job: {
          description: ''
        },
        user: {
          email: '',
          user_profile_attributes: {}
        }
      },
      jobStep: 1,
      jobRequestErrors: null,
      jobValidated: false,
      isSendingRequest: false,
      createdJob: null
    };
  }

  sendJobRequest(state, action) {
    let newState = Object.assign({}, state);

    newState.isSendingRequest = true;

    APIClient.post('jobs', {
      data: action.jobFormData,

      done(response) {
        AppDispatcher.dispatch({
          type: 'SEND_JOB_REQUEST_DONE',
          response
        });
      },

      fail(xhr) {
        AppDispatcher.dispatch({
          type: 'SEND_JOB_REQUEST_FAIL',
          errors: xhr.responseJSON.errors || [xhr.responseJSON.error]
        });
      }
    });

    return newState;
  }

  sendJobRequestDone(state, action) {
    let newState = Object.assign({}, state);

    newState.jobRequestErrors = null;
    newState.isSendingRequest = false;
    newState.createdJob       = action.response;

    return newState;
  }

  sendJobRequestFail(state, action) {
    let newState = Object.assign({}, state);

    newState.jobRequestErrors = action.errors;
    newState.isSendingRequest = false;
    newState.createdJob       = null;

    return newState;
  }

  validateJobRequest(state, action) {
    let newState = Object.assign({}, state);

    newState.isSendingRequest = true;

    APIClient.post('jobs', {
      data: {
        job: action.jobFormData.job,
        validate_only: true
      },

      done(response) {
        AppDispatcher.dispatch({
          type: 'VALIDATE_JOB_REQUEST_DONE',
          response
        });
      },

      fail(xhr) {
        AppDispatcher.dispatch({
          type: 'VALIDATE_JOB_REQUEST_FAIL',
          errors: xhr.responseJSON.errors || [xhr.responseJSON.error]
        });
      }
    });

    return newState;
  }

  validateJobRequestDone(state, action) {
    let newState = Object.assign({}, state);

    newState.jobRequestErrors = null;
    newState.isSendingRequest = false;
    newState.jobValidated     = true;

    return newState;
  }

  validateJobRequestFail(state, action) {
    let newState = Object.assign({}, state);

    newState.jobRequestErrors = action.errors;
    newState.isSendingRequest = false;
    newState.jobValidated     = false;

    return newState;
  }

  reduce(state, action) {
    switch (action.type) {
      case 'SEND_JOB_REQUEST':
        return this.sendJobRequest(state, action);

      case 'SEND_JOB_REQUEST_DONE':
        return this.sendJobRequestDone(state, action);

      case 'SEND_JOB_REQUEST_FAIL':
        return this.sendJobRequestFail(state, action);

      case 'VALIDATE_JOB_REQUEST':
        return this.validateJobRequest(state, action);

      case 'VALIDATE_JOB_REQUEST_DONE':
        return this.validateJobRequestDone(state, action);

      case 'VALIDATE_JOB_REQUEST_FAIL':
        return this.validateJobRequestFail(state, action);

      default:
        return state;
    }
  }
}

const NewJobStore = new NewJobStoreClass();
