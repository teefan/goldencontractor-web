class ServiceAndTypeStoreClass extends FluxUtils.ReduceStore {
  constructor() {
    super(AppDispatcher);
  }

  getInitialState() {
    return {
      constructionServices: [],
      constructionTypes:    [],
      isSendingRequest:     false
    };
  }

  loadConstructionServices(state, action) {
    let newState = Object.assign({}, state);

    newState.isSendingRequest = true;

    APIClient.get('construction_services', {
      done: (response) => {
        AppDispatcher.dispatch({
          type: 'LOAD_CONSTRUCTION_SERVICES_DONE',
          response
        });
      }
    });

    return newState;
  }

  loadConstructionServicesDone(state, action) {
    let newState = Object.assign({}, state);

    newState.isSendingRequest = false;
    newState.constructionServices = action.response;

    return newState;
  }

  loadConstructionTypes(state, action) {
    let newState = Object.assign({}, state);

    newState.isSendingRequest = true;

    APIClient.get('construction_types', {
      done: (response) => {
        AppDispatcher.dispatch({
          type: 'LOAD_CONSTRUCTION_TYPES_DONE',
          response
        });
      }
    });

    return newState;
  }

  loadConstructionTypesDone(state, action) {
    let newState = Object.assign({}, state);

    newState.isSendingRequest = false;
    newState.constructionTypes = action.response;

    return newState;
  }

  reduce(state, action) {
    switch (action.type) {
      case 'LOAD_CONSTRUCTION_SERVICES':
        return this.loadConstructionServices(state, action);

      case 'LOAD_CONSTRUCTION_SERVICES_DONE':
        return this.loadConstructionServicesDone(state, action);

      case 'LOAD_CONSTRUCTION_TYPES':
        return this.loadConstructionTypes(state, action);

      case 'LOAD_CONSTRUCTION_TYPES_DONE':
        return this.loadConstructionTypesDone(state, action);

      default:
        return state;
    }
  }
}

const ServiceAndTypeStore = new ServiceAndTypeStoreClass();
