class AuthenticationStoreClass extends FluxUtils.ReduceStore {
  constructor() {
    super(AppDispatcher);
  }

  getInitialState() {
    return {
      signUpFormData: {
        home_owner: {
          user: {
            user_profile_attributes: {
              name: ''
            },
            email: '',
            password: ''
          }
        },
        contractor: {
          user: {
            user_profile_attributes: {
              name: '',
              phone_number: ''
            },
            company_attributes:      {
              name: '',
              tax_number: '',
              street_address: ''
            },
            email: '',
            password: ''
          }
        }
      },
      signUpErrors: null,
      signInFormData: {
        user: {
          email: '',
          password: '',
          remember_me: false
        }
      },
      signInErrors: null,
      isSendingRequest: false,
      currentUser: null
    };
  }

  signUp(state, action) {
    let newState = Object.assign({}, state);

    newState.signUpErrors = null;
    newState.isSendingRequest = true;

    APIClient.post('users', {
      data: action.signUpFormData[action.accountType],

      done(response) {
        AppDispatcher.dispatch({
          type: 'SIGN_UP_DONE',
          response
        });
      },

      fail(xhr) {
        AppDispatcher.dispatch({
          type: 'SIGN_UP_FAIL',
          errors: xhr.responseJSON.errors || [xhr.responseJSON.error]
        });
      }
    });

    return newState;
  }

  signUpDone(state, action) {
    let newState = Object.assign({}, state);

    newState.signUpErrors = null;
    newState.isSendingRequest = false;
    newState.currentUser = action.response;

    return newState;
  }

  signUpFail(state, action) {
    let newState = Object.assign({}, state);

    newState.signUpErrors = action.errors;
    newState.isSendingRequest = false;

    return newState;
  }

  signIn(state, action) {
    let newState = Object.assign({}, state);

    newState.signInErrors = null;
    newState.isSendingRequest = true;

    APIClient.post('users/sign_in', {
      data: action.signInFormData,

      done(response) {
        AppDispatcher.dispatch({
          type: 'SIGN_IN_DONE',
          response
        });
      },

      fail(xhr) {
        AppDispatcher.dispatch({
          type: 'SIGN_IN_FAIL',
          errors: xhr.responseJSON.errors || [xhr.responseJSON.error]
        });
      }
    });

    return newState;
  }

  signInDone(state, action) {
    let newState = Object.assign({}, state);

    newState.signInErrors = null;
    newState.isSendingRequest = false;
    newState.currentUser = action.response;

    return newState;
  }

  signInFail(state, action) {
    let newState = Object.assign({}, state);

    newState.signInErrors = action.errors;
    newState.isSendingRequest = false;

    return newState;
  }

  reduce(state, action) {
    switch (action.type) {
      case 'SIGN_UP':
        return this.signUp(state, action);

      case 'SIGN_UP_DONE':
        return this.signUpDone(state, action);

      case 'SIGN_UP_FAIL':
        return this.signUpFail(state, action);

      case 'SIGN_IN':
        return this.signIn(state, action);

      case 'SIGN_IN_DONE':
        return this.signInDone(state, action);

      case 'SIGN_IN_FAIL':
        return this.signInFail(state, action);

      default:
        return state;
    }
  }
}

const AuthenticationStore = new AuthenticationStoreClass();
