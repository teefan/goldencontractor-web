class LocationStoreClass extends FluxUtils.ReduceStore {
  constructor() {
    super(AppDispatcher);
  }

  getInitialState() {
    return {
      countries: [],
      cities:    [],
      districts: [],
      isSendingRequest: false
    };
  }

  loadCountries(state, action) {
    let newState = Object.assign({}, state);

    newState.countries = [];
    newState.isSendingRequest = true;

    APIClient.get('countries', {
      done: (response) => {
        AppDispatcher.dispatch({
          type: 'LOAD_COUNTRIES_DONE',
          response
        });
      }
    });

    return newState;
  }

  loadCountriesDone(state, action) {
    let newState = Object.assign({}, state);

    newState.isSendingRequest = false;
    newState.countries = action.response;

    return newState;
  }

  loadCities(state, action) {
    let newState = Object.assign({}, state);

    newState.cities = [];
    newState.isSendingRequest = true;

    APIClient.get('cities', {
      data: {
        country_id: action.countryID
      },

      done: (response) => {
        AppDispatcher.dispatch({
          type: 'LOAD_CITIES_DONE',
          response
        });
      }
    });

    return newState;
  }

  loadCitiesDone(state, action) {
    let newState = Object.assign({}, state);

    newState.isSendingRequest = false;
    newState.cities = action.response;

    return newState;
  }

  loadDistricts(state, action) {
    let newState = Object.assign({}, state);

    newState.districts = [];
    newState.isSendingRequest = true;

    APIClient.get('districts', {
      data: {
        city_id: action.cityID
      },

      done: (response) => {
        AppDispatcher.dispatch({
          type: 'LOAD_DISTRICTS_DONE',
          response
        });
      }
    });

    return newState;
  }

  loadDistrictsDone(state, action) {
    let newState = Object.assign({}, state);

    newState.isSendingRequest = false;
    newState.districts = action.response;

    return newState;
  }

  reduce(state, action) {
    switch (action.type) {
      case 'LOAD_COUNTRIES':
        return this.loadCountries(state, action);

      case 'LOAD_COUNTRIES_DONE':
        return this.loadCountriesDone(state, action);

      case 'LOAD_CITIES':
        return this.loadCities(state, action);

      case 'LOAD_CITIES_DONE':
        return this.loadCitiesDone(state, action);

      case 'LOAD_DISTRICTS':
        return this.loadDistricts(state, action);

      case 'LOAD_DISTRICTS_DONE':
        return this.loadDistrictsDone(state, action);

      default:
        return state;
    }
  }
}

const LocationStore = new LocationStoreClass();
