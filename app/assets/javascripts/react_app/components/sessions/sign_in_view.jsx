class SignInView extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="ui middle aligned center aligned grid authentication-box container">
        <SignInForm {...this.props} />
      </div>
    );
  }
}

SignInView.propTypes = {
  requestParams: React.PropTypes.object,
  images:        React.PropTypes.object
};
