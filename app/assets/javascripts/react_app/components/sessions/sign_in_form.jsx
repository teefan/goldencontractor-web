let authenticationListener = null;

let signInFields = {
  'email':       { label: 'Email', formName: 'user[email]' },
  'password':    { label: 'Mật khẩu', formName: 'user[password]' },
  'remember_me': { label: 'Lưu đăng nhập', formName: 'user[remember_me]' }
};

class SignInForm extends React.Component {
  constructor(props) {
    super(props);

    let currentState = AuthenticationStore.getState();

    this.state = {
      signInFormData: currentState.signInFormData,
      signInErrors: currentState.signInErrors,
      isSendingRequest: currentState.isSendingRequest,
      fullErrorMessages: [],
      currentUser: currentState.currentUser
    };

    this.componentDidMount      = this.componentDidMount.bind(this);
    this.componentWillUnmount   = this.componentWillUnmount.bind(this);
    this.handleFieldChange      = this.handleFieldChange.bind(this);
    this.handleFormSubmit       = this.handleFormSubmit.bind(this);
    this.onAuthenticationChange = this.onAuthenticationChange.bind(this);
  }

  componentDidMount() {
    let _this = this;

    $('.checkbox').checkbox({
      onChecked: () => {
        let event = {
          target: {
            name: signInFields['remember_me'].formName,
            value: true
          }
        };
        _this.handleFieldChange(event);
      },
      onUnchecked: () => {
        let event = {
          target: {
            name: signInFields['remember_me'].formName,
            value: false
          }
        };
        _this.handleFieldChange(event);
      }
    });

    authenticationListener = AuthenticationStore.addListener(this.onAuthenticationChange);
  }

  componentWillUnmount() {
    if (authenticationListener) authenticationListener.remove();
  }

  handleFieldChange(e) {
    let { name, value } = e.target;

    let { signInFormData, signInErrors } = this.state;

    switch (name) {
      case 'user[email]':
        signInFormData.user.email = value;
        break;
      case 'user[password]':
        signInFormData.user.password = value;
        break;
      case 'user[remember_me]':
        signInFormData.user.remember_me = value;
        break;
    }

    this.setState({ signInFormData, signInErrors });
  }

  handleFormSubmit(e) {
    e.preventDefault();

    let { signInFormData } = this.state;

    AuthenticationActions.signIn(signInFormData);
  }

  onAuthenticationChange() {
    let newState = AuthenticationStore.getState();

    let signInFormData = newState.signInFormData;
    let signInErrors = newState.signInErrors;

    let fullErrorMessages = APIClient.buildFullMessages(signInErrors, signInFields);

    // sign in successfully
    if (!signInErrors && newState.currentUser) {
      newState.currentUser['remember_me'] = signInFormData.user.remember_me;
      Authentication.storeUser(newState.currentUser);
      location.href = '/';
    }

    this.setState({
      signInFormData,
      signInErrors,
      isSendingRequest: newState.isSendingRequest,
      fullErrorMessages,
      currentUser: newState.currentUser
    });
  }

  render() {
    let { images } = this.props;

    let { signInFormData, signInErrors, isSendingRequest, fullErrorMessages } = this.state;

    let formClass = classNames('ui', 'large', 'form', { error: !!signInErrors });

    let submitButtonClass = classNames('ui', 'fluid', 'primary', 'big', 'button', { loading: isSendingRequest });

    return (
      <div className="six wide computer sixteen wide mobile column">
        <div className="ui padded sign-up-box segment">
          <h1><img src={images.logo} /></h1>
          <form className={formClass} method="POST" onSubmit={this.handleFormSubmit}>
            <ErrorMessagesBox errorMessages={fullErrorMessages} />

            <div className="field">
              <input type="email" autoFocus="true"
                     name={signInFields['email'].formName}
                     placeholder={signInFields['email'].label}
                     value={signInFormData.user.email}
                     onChange={this.handleFieldChange} />
            </div>

            <div className="field">
              <input type="password"
                     name={signInFields['password'].formName}
                     placeholder={signInFields['password'].label}
                     value={signInFormData.user.password}
                     onChange={this.handleFieldChange} />
            </div>

            <div className="fields">
              <div className="field">
                <div className="ui checkbox">
                  <input type="checkbox" id="user_remember_me" tabIndex="0" className="hidden"
                         name={signInFields['remember_me'].formName}
                         value={signInFormData.user.remember_me}
                         onChange={this.handleFieldChange} />
                  <label>{signInFields['remember_me'].label}</label>
                </div>
              </div>
            </div>

            <button className={submitButtonClass} type="submit">ĐĂNG NHẬP</button>
            <br />
            <p>Bạn chưa có tài khoản?<br /><a href="/dang_ky">Đăng kí tài khoản miễn phí.</a></p>
          </form>
        </div>
      </div>
    );
  }
}

SignInForm.propTypes = {
  requestParams: React.PropTypes.object,
  images:        React.PropTypes.object
};
