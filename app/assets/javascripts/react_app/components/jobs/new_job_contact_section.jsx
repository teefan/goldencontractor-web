class NewJobContactSection extends React.Component {
  constructor(props) {
    super(props);

    this.handlePrevStepClick = this.handlePrevStepClick.bind(this);
  }

  handlePrevStepClick(e) {
    e.preventDefault();

    let { goToStep } = this.props;

    goToStep(1);
  }

  render() {
    let { newJobState, handleFieldChange } = this.props;

    let { jobFormData, isSendingRequest } = newJobState;

    let submitButtonClass = classNames('ui', 'primary', 'button', { loading: isSendingRequest });

    return (
      <div className="row">
        <div className="column">
          <div className="ui very padded segment">
            <div className="ui stackable grid">
              <div className="row">
                <div className="seven wide column">
                  <div className="two fields">
                    <div className="six wide field"><label>Họ và Tên</label></div>
                    <div className="ten wide field">
                      <input type="text"
                             name={newContactFields['user_profile.name'].formName}
                             placeholder={newContactFields['user_profile.name'].label}
                             value={jobFormData.user.user_profile_attributes.name}
                             onChange={handleFieldChange} />
                    </div>
                  </div>

                  <div className="two fields">
                    <div className="six wide field"><label>Email</label></div>
                    <div className="ten wide field">
                      <input type="text"
                             name={newContactFields['email'].formName}
                             placeholder={newContactFields['email'].label}
                             value={jobFormData.user.email}
                             onChange={handleFieldChange} />
                    </div>
                  </div>

                  <div className="two fields">
                    <div className="six wide field"><label>Số điện thoại</label></div>
                    <div className="ten wide field">
                      <input type="text"
                             name={newContactFields['user_profile.phone_number'].formName}
                             placeholder={newContactFields['user_profile.phone_number'].label}
                             value={jobFormData.user.user_profile_attributes.phone_number}
                             onChange={handleFieldChange} />
                    </div>
                  </div>

                  <div className="two fields">
                    <div className="six wide field"><label>Năm sinh</label></div>
                    <div className="ten wide field">
                      <input type="text"
                             name={newContactFields['user_profile.date_of_birth'].formName}
                             placeholder={newContactFields['user_profile.date_of_birth'].label}
                             value={jobFormData.user.user_profile_attributes.date_of_birth}
                             onChange={handleFieldChange} />
                    </div>
                  </div>

                  <div className="two fields">
                    <div className="six wide field"><label>Thời điểm liên lạc</label></div>
                    <div className="ten wide field">
                      <input type="text"
                             name={newContactFields['user_profile.contact_time'].formName}
                             placeholder={newContactFields['user_profile.contact_time'].label}
                             value={jobFormData.user.user_profile_attributes.contact_time}
                             onChange={handleFieldChange} />
                    </div>
                  </div>
                </div>

                <div className="nine wide column">
                  <p className="text muted small">* Thông tin của bạn sẽ được bảo mật tại NHATHAUVANG.com.</p>
                  <p className="text muted small">
                    Chỉ các Nhà Thầu là thành viên được xem được thông tin yêu cầu
                    mà Bạn cần thực hiện.
                  </p>
                </div>
              </div>
            </div>
          </div>

          <a href="javascript:void(0)" className="ui basic primary button" onClick={this.handlePrevStepClick}>
            QUAY TRỞ LẠI
          </a>
          <button className={submitButtonClass} type="submit">GỬI YÊU CẦU</button>
        </div>
      </div>
    );
  }
}

NewJobContactSection.propTypes = {
  goToStep:          React.PropTypes.func.isRequired,
  handleFieldChange: React.PropTypes.func.isRequired,
  newJobState:       React.PropTypes.object.isRequired
};
