class NewJobStep2 extends  React.Component {
  render() {
    return (
      <div className="new-job-step-content new-job-step-2">
        <div className="ui vertically divided grid stackable container">
          <NewJobContactSection {...this.props} />
        </div>
      </div>
    );
  }
}

NewJobStep2.propTypes = {
  goToStep:          React.PropTypes.func.isRequired,
  newJobState:       React.PropTypes.object.isRequired,
  handleFieldChange: React.PropTypes.func.isRequired
};
