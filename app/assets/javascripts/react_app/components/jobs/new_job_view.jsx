let newJobListener      = null;
let locationListener    = null;
let serviceTypeListener = null;

class NewJobView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      newJobState: NewJobStore.getState(),
      locationState: LocationStore.getState(),
      serviceAndTypeState: ServiceAndTypeStore.getState()
    };

    this.componentDidMount    = this.componentDidMount.bind(this);
    this.componentWillUnmount = this.componentWillUnmount.bind(this);
    this.onNewJobChange       = this.onNewJobChange.bind(this);
    this.onLocationChange     = this.onLocationChange.bind(this);
    this.onServiceTypeChange  = this.onServiceTypeChange.bind(this);
    this.goToStep             = this.goToStep.bind(this);
  }

  componentDidMount() {
    newJobListener = NewJobStore.addListener(this.onNewJobChange);
    locationListener = LocationStore.addListener(this.onLocationChange);
    serviceTypeListener = ServiceAndTypeStore.addListener(this.onServiceTypeChange);

    // first country is Viet Nam
    LocationActions.loadCities(1);
    ServiceAndTypeActions.loadConstructionServices();
    ServiceAndTypeActions.loadConstructionTypes();
  }

  componentWillUnmount() {
    if (newJobListener) newJobListener.remove();
    if (locationListener) locationListener.remove();
    if (serviceTypeListener) serviceTypeListener.remove();
  }

  onNewJobChange() {
    let newJobState = NewJobStore.getState();

    let { jobValidated, jobStep, jobRequestErrors, createdJob } = newJobState;
    // job created successfully
    if (!jobRequestErrors && createdJob) {
      location.href = '/';
    }

    if (jobValidated && jobStep === 1) {
      newJobState.jobStep = 2;
    }

    this.setState({
      newJobState: newJobState
    });
  }

  onLocationChange() {
    let newLocationState = LocationStore.getState();

    this.setState({
      locationState: newLocationState
    });
  }

  onServiceTypeChange() {
    let newServiceAndTypeState = ServiceAndTypeStore.getState();

    this.setState({
      serviceAndTypeState: newServiceAndTypeState
    });
  }

  goToStep(stepNumber) {
    let { newJobState } = this.state;

    newJobState.jobStep = parseInt(stepNumber);

    this.setState({
      jobState: newJobState
    });
  }

  render() {
    let { newJobState, locationState, serviceAndTypeState } = this.state;

    let jobStep = newJobState.jobStep;

    return (
      <div className="new-job-view">
        <NewJobNav jobStep={jobStep} />

        <NewJobForm {...this.props} newJobState={newJobState} locationState={locationState}
                    serviceAndTypeState={serviceAndTypeState} goToStep={this.goToStep} />
      </div>
    );
  }
}
