class NewJobTypeSection extends React.Component {
  constructor(props) {
    super(props);

    this.componentDidMount                = this.componentDidMount.bind(this);
    this.renderConstructionServicesSelect = this.renderConstructionServicesSelect.bind(this);
    this.renderConstructionTypesSelect    = this.renderConstructionTypesSelect.bind(this);
  }

  componentDidMount() {
    let { handleFieldChange } = this.props;

    $('.ui.dropdown#job_construction_service_id').dropdown({
      onChange: function(value, text, $selectedItem) {
        let event = { target: { name: newJobFields['construction_service_id'].formName, value: value } };
        handleFieldChange(event);
      }
    });
    $('.ui.dropdown#job_construction_type_id').dropdown({
      onChange: function(value, text, $selectedItem) {
        let event = { target: { name: newJobFields['construction_type_id'].formName, value: value } };
        handleFieldChange(event);
      }
    });
  }

  renderConstructionServicesSelect() {
    let { newJobState, serviceAndTypeState } = this.props;

    let { jobFormData } = newJobState;

    let constructionServices = serviceAndTypeState.constructionServices;
    let selectItems = [];
    for (let i = 0; i < constructionServices.length; i++) {
      let constructionService = constructionServices[i];
      let selectItem = <div className="item" key={`c-service-${constructionService.id}`}
                            data-value={constructionService.id}>{constructionService.name}</div>;
      selectItems.push(selectItem);
    }

    let constructionServicesSelect =
      <div id="job_construction_service_id" className="ui selection dropdown">
        <input type="hidden" name={newJobFields['construction_service_id'].formName}
               value={jobFormData.job.construction_service_id} />

        <i className="dropdown icon" />
        <div className="default text">Chọn dịch vụ</div>
        <div className="menu">
          {selectItems}
        </div>
      </div>;

    return constructionServicesSelect;
  }

  renderConstructionTypesSelect() {
    let { newJobState, serviceAndTypeState } = this.props;

    let { jobFormData } = newJobState;

    let constructionTypes = serviceAndTypeState.constructionTypes;
    let selectItems = [];
    for (let i = 0; i < constructionTypes.length; i++) {
      let constructionType = constructionTypes[i];
      let selectItem = <div className="item" key={`c-type-${constructionType.id}`}
                            data-value={constructionType.id}>{constructionType.name}</div>;
      selectItems.push(selectItem);
    }

    let constructionTypesSelect =
          <div id="job_construction_type_id" className="ui selection dropdown">
            <input type="hidden" name={newJobFields['construction_type_id'].formName}
                   value={jobFormData.job.construction_type_id} />

            <i className="dropdown icon" />
            <div className="default text">Chọn loại công trình</div>
            <div className="menu">
              {selectItems}
            </div>
          </div>;

    return constructionTypesSelect;
  }

  render() {
    let { newJobState } = this.props;

    let { jobRequestErrors } = newJobState;

    let csFieldClass = classNames('ten wide field', {
      error: !!(jobRequestErrors && jobRequestErrors['construction_service_id'])
    });
    let ctFieldClass = classNames('ten wide field', {
      error: !!(jobRequestErrors && jobRequestErrors['construction_type_id'])
    });

    return (
      <div className="two column new-job-type-section row">
        <div className="column">
          <div className="two fields">
            <div className="six wide field"><label>Loại dịch vụ</label></div>

            <div className={csFieldClass}>
              {this.renderConstructionServicesSelect()}
            </div>
          </div>

          <div className="two fields">
            <div className="six wide field"><label>Loại công trình</label></div>
            <div className={ctFieldClass}>
              {this.renderConstructionTypesSelect()}
            </div>
          </div>
        </div>

        <div className="column">
          <p className="text muted small">* Bạn hoàn toàn được miễn phí khi yêu cầu tư vấn xây, sửa nhà.</p>
          <p className="text muted small">NHATHAUVANG.com sẽ cung cấp cho bạn 3 đơn vị xây dựng phù hợp.</p>
        </div>
      </div>
    );
  }
}

NewJobTypeSection.propTypes = {
  newJobState:         React.PropTypes.object.isRequired,
  serviceAndTypeState: React.PropTypes.object.isRequired,
  handleFieldChange:   React.PropTypes.func.isRequired
};
