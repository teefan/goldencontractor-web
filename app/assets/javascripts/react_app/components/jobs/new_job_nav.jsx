class NewJobNav extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    let { jobStep } = this.props;

    let step1Class = classNames('item', 'new-job-step', { active: jobStep === 1 });
    let step2Class = classNames('item', 'new-job-step', { active: jobStep === 2 });

    return (
      <div className="new-job-nav">
        <div className="ui two column grid stackable new-job-steps container">
          <div className="row">
            <div className="column">
              <div className={step1Class}>1. Dịch Vụ Yêu Cầu</div>
            </div>

            <div className="column">
              <div className={step2Class}>2. Thông Tin Liên Hệ</div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

NewJobNav.propTypes = {
  jobStep: React.PropTypes.number
};
