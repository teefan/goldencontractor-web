class NewJobDetails extends React.Component {
  constructor(props) {
    super(props);

    this.componentDidMount   = this.componentDidMount.bind(this);
    this.handleNextStepClick = this.handleNextStepClick.bind(this);
    this.renderCitiesSelect  = this.renderCitiesSelect.bind(this);
  }

  componentDidMount() {
    let { handleFieldChange } = this.props;

    $('.ui.dropdown#job_city_id').dropdown({
      onChange: function(value, text, $selectedItem) {
        $('.ui.dropdown#job_district_id').dropdown('clear');
        LocationActions.loadDistricts(parseInt(value));

        let event = { target: { name: newJobFields['city_id'].formName, value: value } };
        handleFieldChange(event);
      }
    });
    $('.ui.dropdown#job_district_id').dropdown({
      onChange: function(value, text, $selectedItem) {
        let event = { target: { name: newJobFields['district_id'].formName, value: value } };
        handleFieldChange(event);
      }
    });

    $.datetimepicker.setLocale('vi');
    $('#job_planned_start_date').datetimepicker({
      timepicker: false,
      format:'d/m/Y',
      formatDate: 'Y/m/d',
      onChangeDateTime:function(date, $input) {
        let value = $input.val();

        let event = { target: { name: newJobFields['planned_start_date'].formName, value: value } };
        handleFieldChange(event);
      }
    });
  }

  renderCitiesSelect() {
    let { newJobState, locationState } = this.props;

    let { jobFormData } = newJobState;

    let cities = locationState.cities;
    let selectItems = [];
    for (let i = 0; i < cities.length; i++) {
      let city = cities[i];
      let selectItem = <div className="item" key={`city-${city.id}`}
                            data-value={city.id}>{city.name}</div>;
      selectItems.push(selectItem);
    }

    let citiesSelect =
      <div id="job_city_id" className="ui selection dropdown">
        <input type="hidden" name={newJobFields['city_id'].formName} value={jobFormData.job.city_id} />

        <i className="dropdown icon" />
        <div className="default text">Chọn tỉnh thành</div>
        <div className="menu">
          {selectItems}
        </div>
      </div>;

    return citiesSelect;
  }

  renderDistrictsSelect() {
    let { newJobState, locationState } = this.props;

    let { jobFormData } = newJobState;

    let districts = locationState.districts;
    let selectItems = [];
    for (let i = 0; i < districts.length; i++) {
      let district = districts[i];
      let selectItem = <div className="item" key={`district-${district.id}`}
                            data-value={district.id}>{district.name}</div>;
      selectItems.push(selectItem);
    }

    let districtsSelect =
          <div id="job_district_id" className="ui selection dropdown">
            <input type="hidden" name={newJobFields['district_id'].formName} value={jobFormData.job.district_id} />

            <i className="dropdown icon" />
            <div className="default text">Chọn khu vực</div>
            <div className="menu">
              {selectItems}
            </div>
          </div>;

    return districtsSelect;
  }

  handleNextStepClick(e) {
    e.preventDefault();

    let { newJobState } = this.props;

    let { jobFormData } = newJobState;

    JobActions.validateJobRequest(jobFormData);
  }

  render() {
    let { newJobState, handleFieldChange } = this.props;

    let { jobFormData, jobRequestErrors, isSendingRequest } = newJobState;

    let cityFieldClass = classNames('ten wide field', {
      error: !!(jobRequestErrors && jobRequestErrors['city_id'])
    });
    let districtFieldClass = classNames('ten wide field', {
      error: !!(jobRequestErrors && jobRequestErrors['district_id'])
    });
    let addressFieldClass = classNames('ten wide field', {
      error: !!(jobRequestErrors && jobRequestErrors['address'])
    });
    let landAreaFieldClass = classNames('ten wide field', {
      error: !!(jobRequestErrors && jobRequestErrors['land_area'])
    });
    let landLengthFieldClass = classNames('ten wide field', {
      error: !!(jobRequestErrors && jobRequestErrors['land_length'])
    });
    let landWidthFieldClass = classNames('ten wide field', {
      error: !!(jobRequestErrors && jobRequestErrors['land_width'])
    });
    let startDateFieldClass = classNames('ten wide field', {
      error: !!(jobRequestErrors && jobRequestErrors['planned_start_date'])
    });
    let budgetFieldClass = classNames('ten wide field', {
      error: !!(jobRequestErrors && jobRequestErrors['budget'])
    });
    let descFieldClass = classNames('ten wide field', {
      error: !!(jobRequestErrors && jobRequestErrors['description'])
    });

    let submitButtonClass = classNames('ui', 'primary', 'button', { loading: isSendingRequest });

    return (
      <div className="row">
        <div className="column">
          <h5>Chi tiết yêu cầu</h5>

          <div className="ui padded segment">
            <div className="ui two column stackable grid">
              <div className="column">
                <div className="two fields">
                  <div className="six wide field"><label>Tỉnh thành</label></div>
                  <div className={cityFieldClass}>{this.renderCitiesSelect()}</div>
                </div>

                <div className="two fields">
                  <div className="six wide field"><label>Khu vực</label></div>
                  <div className={districtFieldClass}>{this.renderDistrictsSelect()}</div>
                </div>

                <div className="two fields">
                  <div className="six wide field"><label>Địa chỉ</label></div>
                  <div className={addressFieldClass}>
                    <input type="text"
                           name={newJobFields['address'].formName}
                           placeholder={newJobFields['address'].label}
                           value={jobFormData.job.address}
                           onChange={handleFieldChange} />
                  </div>
                </div>

                <div className="two fields">
                  <div className="six wide field"><label>Diện tích đất</label></div>
                  <div className={landAreaFieldClass}>
                    <div className="ui right labeled input">
                      <input type="number"
                             name={newJobFields['land_area'].formName}
                             placeholder={newJobFields['land_area'].label}
                             value={jobFormData.job.land_area}
                             onChange={handleFieldChange} />
                      <div className="ui basic label">m<sup>2</sup></div>
                    </div>
                  </div>
                </div>

                <div className="two fields">
                  <div className="six wide field"><label>Chiều dài</label></div>
                  <div className={landLengthFieldClass}>
                    <div className="ui right labeled input">
                      <input type="number"
                             name={newJobFields['land_length'].formName}
                             placeholder={newJobFields['land_length'].label}
                             value={jobFormData.job.land_length}
                             onChange={handleFieldChange} />
                      <div className="ui basic label">m</div>
                    </div>
                  </div>
                </div>

                <div className="two fields">
                  <div className="six wide field"><label>Chiều rộng</label></div>
                  <div className={landWidthFieldClass}>
                    <div className="ui right labeled input">
                      <input type="number"
                             name={newJobFields['land_width'].formName}
                             placeholder={newJobFields['land_width'].label}
                             value={jobFormData.job.land_width}
                             onChange={handleFieldChange} />
                      <div className="ui basic label">m</div>
                    </div>
                  </div>
                </div>

                <div className="two fields">
                  <div className="six wide field"><label>Ngày dự định thi công</label></div>
                  <div className={startDateFieldClass}>
                    <input type="text" id="job_planned_start_date"
                           name={newJobFields['planned_start_date'].formName}
                           placeholder={newJobFields['planned_start_date'].label}
                           value={jobFormData.job.planned_start_date}
                           onChange={handleFieldChange} />
                  </div>
                </div>

                <div className="two fields">
                  <div className="six wide field"><label>Chi phí dự kiến</label></div>
                  <div className={budgetFieldClass}>
                    <div className="ui right labeled input">
                      <input type="number"
                             name={newJobFields['budget'].formName}
                             placeholder={newJobFields['budget'].label}
                             value={jobFormData.job.budget}
                             onChange={handleFieldChange} />
                      <div className="ui basic label">triệu đồng</div>
                    </div>
                  </div>
                </div>

                <div className="two fields">
                  <div className="six wide field"><label>Mô tả chi tiết</label></div>
                  <div className={descFieldClass}>
                    <textarea rows="5"
                              name={newJobFields['description'].formName}
                              placeholder={newJobFields['description'].label}
                              value={jobFormData.job.description}
                              onChange={handleFieldChange} />
                  </div>
                </div>
              </div>

              <div className="column">
                <p className="text muted small">
                  * Chúng tôi khuyến khích Bạn mô tả chi tiết yêu cầu, đăng hình ảnh và ngân sách dự kiến nhằm giúp
                  Nhà Thầu dễ dàng hiểu rõ hơn phạm vi công việc mà bạn cần thực hiện.
                </p>

                <p className="text muted small">
                  Điều này sẽ giúp bạn tiết kiệm thời gian trong việc tìm thấy Nhà Thầu phù hợp.
                  NHATHAUVANG.com sẽ tư vấn Bạn cập nhật lại Công việc của mình nếu sau 3 ngày gửi yêu cầu mà vẫn chưa
                  có Nhà thầu nào đăng ký liên hệ với Bạn.
                </p>
              </div>
            </div>
          </div>

          <a href="javascript:void(0)" className={submitButtonClass} onClick={this.handleNextStepClick}>TIẾP THEO</a>
        </div>
      </div>
    );
  }
}

NewJobDetails.propTypes = {
  goToStep:          React.PropTypes.func.isRequired,
  handleFieldChange: React.PropTypes.func.isRequired,
  newJobState:       React.PropTypes.object.isRequired,
  locationState:     React.PropTypes.object.isRequired
};
