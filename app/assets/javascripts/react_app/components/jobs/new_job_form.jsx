let newJobFields = {
  'construction_service_id': { label: 'Loại dịch vụ', formName: 'job[construction_service_id]' },
  'construction_type_id':    { label: 'Loại công trình', formName: 'job[construction_type_id]' },
  'city_id':                 { label: 'Tỉnh thành', formName: 'job[city_id]' },
  'district_id':             { label: 'Khu vực', formName: 'job[district_id]' },
  'address':                 { label: 'Địa chỉ', formName: 'job[address]' },
  'land_area':               { label: 'Diện tích đất', formName: 'job[land_area]' },
  'land_length':             { label: 'Chiều dài', formName: 'job[land_length]' },
  'land_width':              { label: 'Chiều rộng', formName: 'job[land_width]' },
  'planned_start_date':      { label: 'Ngày dự định thi công', formName: 'job[planned_start_date]' },
  'budget':                  { label: 'Chi phí dự kiến', formName: 'job[budget]' },
  'description':             { label: 'Mô tả chi tiết', formName: 'job[description]' }
};

let newContactFields = {
  'email':                      { label: 'Email', formName: 'user[email]' },
  'user_profile':               { label: 'Thông tin người dùng', formName: 'user[user_profile_attributes]' },
  'user_profile.name':          { label: 'Họ và Tên', formName: 'user[user_profile_attributes[name]]' },
  'user_profile.phone_number':  { label: 'Số điện thoại', formName: 'user[user_profile_attributes[phone_number]]' },
  'user_profile.date_of_birth': { label: 'Năm sinh', formName: 'user[user_profile_attributes[date_of_birth]]' },
  'user_profile.contact_time':  { label: 'Thời điểm liên lạc', formName: 'user[user_profile_attributes[contact_time]]' }
};

class NewJobForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      fullErrorMessages: []
    };

    this.componentDidMount   = this.componentDidMount.bind(this);
    this.componentWillUmount = this.componentWillUmount.bind(this);
    this.renderNewJobSteps   = this.renderNewJobSteps.bind(this);
    this.handleFieldChange   = this.handleFieldChange.bind(this);
    this.handleFormSubmit    = this.handleFormSubmit.bind(this);
  }

  componentDidMount() {}

  componentWillUmount() {}

  handleFieldChange(e) {
    let { name, value } = e.target;

    let  { newJobState } = this.props;

    let { jobFormData, jobRequestErrors } = newJobState;

    switch (name) {
      case 'job[construction_service_id]':
        jobFormData.job.construction_service_id = value;
        if (jobRequestErrors) {
          delete jobRequestErrors['construction_service_id'];
        }
        break;
      case 'job[construction_type_id]':
        jobFormData.job.construction_type_id = value;
        if (jobRequestErrors) {
          delete jobRequestErrors['construction_type_id'];
        }
        break;
      case 'job[city_id]':
        jobFormData.job.city_id = value;
        if (jobRequestErrors) {
          delete jobRequestErrors['city_id'];
        }
        break;
      case 'job[district_id]':
        jobFormData.job.district_id = value;
        if (jobRequestErrors) {
          delete jobRequestErrors['district_id'];
        }
        break;
      case 'job[address]':
        jobFormData.job.address = value;
        if (jobRequestErrors) {
          delete jobRequestErrors['address'];
        }
        break;
      case 'job[land_area]':
        jobFormData.job.land_area = value;
        if (jobRequestErrors) {
          delete jobRequestErrors['land_area'];
        }
        break;
      case 'job[land_length]':
        jobFormData.job.land_length = value;
        if (jobRequestErrors) {
          delete jobRequestErrors['land_length'];
        }
        break;
      case 'job[land_width]':
        jobFormData.job.land_width = value;
        if (jobRequestErrors) {
          delete jobRequestErrors['land_width'];
        }
        break;
      case 'job[planned_start_date]':
        jobFormData.job.planned_start_date = value;
        if (jobRequestErrors) {
          delete jobRequestErrors['planned_start_date'];
        }
        break;
      case 'job[budget]':
        jobFormData.job.budget = value;
        if (jobRequestErrors) {
          delete jobRequestErrors['budget'];
        }
        break;
      case 'job[description]':
        jobFormData.job.description = value;
        if (jobRequestErrors) {
          delete jobRequestErrors['description'];
        }
        break;
      case 'user[email]':
        jobFormData.user.email = value;
        break;
      case 'user[user_profile_attributes[name]]':
        jobFormData.user.user_profile_attributes.name = value;
        break;
      case 'user[user_profile_attributes[phone_number]]':
        jobFormData.user.user_profile_attributes.phone_number = value;
        break;
      case 'user[user_profile_attributes[date_of_birth]]':
        jobFormData.user.user_profile_attributes.date_of_birth = value;
        break;
      case 'user[user_profile_attributes[contact_time]]':
        jobFormData.user.user_profile_attributes.contact_time = value;
        break;
    }

    let fullErrorMessages = APIClient.buildFullMessages(jobRequestErrors, newJobFields);

    this.setState({ jobFormData, jobRequestErrors, fullErrorMessages });

    Logger.log('handleFieldChange', name, value);
  }

  handleFormSubmit(e) {
    e.preventDefault();

    let { newJobState } = this.props;

    let { jobFormData } = newJobState;

    JobActions.sendJobRequest(jobFormData);
  }

  renderNewJobSteps() {
    let { goToStep, newJobState, locationState, serviceAndTypeState } = this.props;

    let { jobStep } = newJobState;

    switch (jobStep) {
      case 1:
        return <NewJobStep1 goToStep={goToStep} newJobState={newJobState} locationState={locationState}
                            serviceAndTypeState={serviceAndTypeState} handleFieldChange={this.handleFieldChange} />;
      case 2:
        return <NewJobStep2 goToStep={goToStep} newJobState={newJobState} locationState={locationState}
                            serviceAndTypeState={serviceAndTypeState} handleFieldChange={this.handleFieldChange} />;
    }
  }

  render() {
    let { newJobState } = this.props;

    let { jobRequestErrors } = newJobState;

    let formClass = classNames('ui', 'form', { error: !!jobRequestErrors });

    return (
      <form className={formClass} method="POST" onSubmit={this.handleFormSubmit}>
        {this.renderNewJobSteps()}
      </form>
    );
  }
}

NewJobForm.propTypes = {
  goToStep:            React.PropTypes.func.isRequired,
  newJobState:         React.PropTypes.object.isRequired,
  locationState:       React.PropTypes.object.isRequired,
  serviceAndTypeState: React.PropTypes.object.isRequired
};
