class NewJobStep1 extends  React.Component {
  render() {
    let { newJobState } = this.props;

    let { jobRequestErrors } = newJobState;

    let fullErrorMessages = APIClient.buildFullMessages(jobRequestErrors, newJobFields);

    return (
      <div className="new-job-step-content new-job-step-1">
        <div className="ui grid container">
          {
            fullErrorMessages && fullErrorMessages.length > 0 &&
            <div className="column">
              <ErrorMessagesBox errorMessages={fullErrorMessages} />
            </div>
          }
        </div>

        <div className="ui vertically divided grid stackable container">
          <NewJobTypeSection {...this.props} />

          <NewJobDetails {...this.props} />
        </div>
      </div>
    );
  }
}

NewJobStep1.propTypes = {
  goToStep:          React.PropTypes.func.isRequired,
  newJobState:       React.PropTypes.object.isRequired,
  handleFieldChange: React.PropTypes.func.isRequired
};
