let authenticationListener = null;

let signUpContractorFields = {
  'email':                     { label: 'Email', formName: 'user[email]' },
  'password':                  { label: 'Mật khẩu', formName: 'user[password]' },
  'user_profile':              { label: 'Thông tin người dùng', formName: 'user[user_profile_attributes]' },
  'user_profile.name':         { label: 'Họ và Tên', formName: 'user[user_profile_attributes[name]]' },
  'user_profile.phone_number': { label: 'Số điện thoại', formName: 'user[user_profile_attributes[phone_number]]' },
  'company':                   { label: 'Thông tin công ty', formName: 'user[company_attributes]' },
  'company.name':              { label: 'Tên công ty', formName: 'user[company_attributes[name]]' },
  'company.tax_number':        { label: 'Mã số thuế', formName: 'user[company_attributes[tax_number]]' },
  'company.street_address':    { label: 'Địa chỉ', formName: 'user[company_attributes[street_address]]' }
};

class SignUpContractorForm extends React.Component {
  constructor(props) {
    super(props);

    let currentState = AuthenticationStore.getState();

    this.state = {
      signUpFormData: {
        contractor: currentState.signUpFormData['contractor']
      },
      signUpErrors: currentState.signUpErrors,
      isSendingRequest: currentState.isSendingRequest,
      fullErrorMessages: [],
      currentUser: currentState.currentUser
    };

    this.componentDidMount      = this.componentDidMount.bind(this);
    this.componentWillUnmount   = this.componentWillUnmount.bind(this);
    this.handleFieldChange      = this.handleFieldChange.bind(this);
    this.handleFormSubmit       = this.handleFormSubmit.bind(this);
    this.onAuthenticationChange = this.onAuthenticationChange.bind(this);
  }

  componentDidMount() {
    authenticationListener = AuthenticationStore.addListener(this.onAuthenticationChange);
  }

  componentWillUnmount() {
    if (authenticationListener) authenticationListener.remove();
  }

  handleFieldChange(e) {
    let { name, value } = e.target;

    let { signUpFormData, signUpErrors } = this.state;

    switch (name) {
      case 'user[user_profile_attributes]':
      case 'user[user_profile_attributes[name]]':
        signUpFormData['contractor'].user.user_profile_attributes.name = value;
        if (signUpErrors) {
          delete signUpErrors['user_profile'];
          delete signUpErrors['user_profile.name'];
        }
        break;
      case 'user[user_profile_attributes[phone_number]]':
        signUpFormData['contractor'].user.user_profile_attributes.phone_number = value;
        if (signUpErrors) {
          delete signUpErrors['user_profile.phone_number'];
        }
        break;
      case 'user[company_attributes]':
      case 'user[company_attributes[name]]':
        signUpFormData['contractor'].user.company_attributes.name = value;
        if (signUpErrors) {
          delete signUpErrors['company'];
          delete signUpErrors['company.name'];
        }
        break;
      case 'user[company_attributes[tax_number]]':
        signUpFormData['contractor'].user.company_attributes.tax_number = value;
        if (signUpErrors) {
          delete signUpErrors['company.tax_number'];
        }
        break;
      case 'user[company_attributes[street_address]]':
        signUpFormData['contractor'].user.company_attributes.street_address = value;
        if (signUpErrors) {
          delete signUpErrors['company.street_address'];
        }
        break;
      case 'user[email]':
        signUpFormData['contractor'].user.email = value;
        if (signUpErrors) {
          delete signUpErrors['email'];
        }
        break;
      case 'user[password]':
        signUpFormData['contractor'].user.password = value;
        if (signUpErrors) {
          delete signUpErrors['password'];
        }
        break;
    }

    let fullErrorMessages = APIClient.buildFullMessages(signUpErrors, signUpContractorFields);

    this.setState({ signUpFormData, signUpErrors, fullErrorMessages });
  }

  handleFormSubmit(e) {
    e.preventDefault();

    let { signUpFormData } = this.state;

    AuthenticationActions.signUp(signUpFormData, 'contractor');
  }

  onAuthenticationChange() {
    let newState = AuthenticationStore.getState();

    let signUpErrors = newState.signUpErrors;

    let fullErrorMessages = APIClient.buildFullMessages(signUpErrors, signUpContractorFields);

    // sign up successfully
    if (!signUpErrors && newState.currentUser) {
      Authentication.storeUser(newState.currentUser);
      location.href = '/';
    }

    this.setState({
      signUpFormData: {
        contractor: newState.signUpFormData['contractor']
      },
      signUpErrors,
      isSendingRequest: newState.isSendingRequest,
      fullErrorMessages,
      currentUser: newState.currentUser
    });
  }

  render() {
    let { images } = this.props;

    let { signUpFormData, signUpErrors, isSendingRequest, fullErrorMessages } = this.state;

    let formClass = classNames('ui', 'large', 'form', { error: !!signUpErrors });

    let nameFieldClass = classNames('field', {
      error: !!(signUpErrors && (signUpErrors['user_profile'] || signUpErrors['user_profile.name']))
    });
    let phoneNumberFieldClass = classNames('field', {
      error: !!(signUpErrors && signUpErrors['user_profile.phone_number'])
    });
    let emailFieldClass = classNames('field', { error: !!(signUpErrors && signUpErrors['email']) });
    let passwordFieldClass = classNames('field', { error: !!(signUpErrors && signUpErrors['password']) });
    let companyNameFieldClass = classNames('field', { error: !!(signUpErrors && signUpErrors['company.name']) });
    let companyTaxNumberFieldClass = classNames('field', {
      error: !!(signUpErrors && signUpErrors['company.tax_number'])
    });
    let companyStreetAddressFieldClass = classNames('field', {
      error: !!(signUpErrors && signUpErrors['company.street_address'])
    });

    let submitButtonClass = classNames('ui', 'fluid', 'primary', 'big', 'button', { loading: isSendingRequest });

    return (
      <div className="twelve wide computer sixteen wide mobile column">
        <div className="ui padded sign-up-box segment">
          <form className={formClass} method="POST" onSubmit={this.handleFormSubmit}>
            <div className="ui two column stackable grid">
              <div className="row">
                <div className="column"><h1><img src={images.logo} /></h1></div>
                <div className="column">
                  <h4 className="ui yellow header">
                    <div className="content">
                      BẠN MUỐN ĐĂNG KÍ LÀM NHÀ THẦU?
                      <div className="sub header">Hãy tạo tài khoản ngay!</div>
                    </div>
                  </h4>
                </div>

                <div className="sixteen wide column">
                  <ErrorMessagesBox errorMessages={fullErrorMessages} />
                </div>
              </div>

              <div className="row">
                <div className="column">
                  <div className={nameFieldClass}>
                    <input type="text" autoFocus="true"
                           name={signUpContractorFields['user_profile.name'].formName}
                           placeholder={signUpContractorFields['user_profile.name'].label}
                           value={signUpFormData['contractor'].user.user_profile_attributes.name}
                           onChange={this.handleFieldChange} />
                  </div>

                  <div className={emailFieldClass}>
                    <input type="email"
                           name={signUpContractorFields['email'].formName}
                           placeholder={signUpContractorFields['email'].label}
                           value={signUpFormData['contractor'].user.email}
                           onChange={this.handleFieldChange} />
                  </div>

                  <div className={passwordFieldClass}>
                    <input type="password"
                           name={signUpContractorFields['password'].formName}
                           placeholder={signUpContractorFields['password'].label}
                           value={signUpFormData['contractor'].user.password}
                           onChange={this.handleFieldChange} />
                  </div>
                </div>

                <div className="column">
                  <div className={companyNameFieldClass}>
                    <input type="text"
                           name={signUpContractorFields['company.name'].formName}
                           placeholder={signUpContractorFields['company.name'].label}
                           value={signUpFormData['contractor'].user.company_attributes.name}
                           onChange={this.handleFieldChange} />
                  </div>

                  <div className={companyTaxNumberFieldClass}>
                    <input type="text"
                           name={signUpContractorFields['company.tax_number'].formName}
                           placeholder={signUpContractorFields['company.tax_number'].label}
                           value={signUpFormData['contractor'].user.company_attributes.tax_number}
                           onChange={this.handleFieldChange} />
                  </div>

                  <div className={phoneNumberFieldClass}>
                    <input type="text"
                           name={signUpContractorFields['user_profile.phone_number'].formName}
                           placeholder={signUpContractorFields['user_profile.phone_number'].label}
                           value={signUpFormData['contractor'].user.user_profile_attributes.phone_number}
                           onChange={this.handleFieldChange} />
                  </div>

                  <div className={companyStreetAddressFieldClass}>
                    <input type="text"
                           name={signUpContractorFields['company.street_address'].formName}
                           placeholder={signUpContractorFields['company.street_address'].label}
                           value={signUpFormData['contractor'].user.company_attributes.street_address}
                           onChange={this.handleFieldChange} />
                  </div>

                  <button className={submitButtonClass} type="submit">ĐĂNG KÍ</button>
                  <br />
                  <p className="small muted text">
                    Việc đăng ký là đồng ý với <a href="/">Điều khoản sử dụng</a> và <a href="/">Quy chế hoạt động</a>.
                  </p>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

SignUpContractorForm.propTypes = {
  requestParams: React.PropTypes.object,
  images:        React.PropTypes.object
};
