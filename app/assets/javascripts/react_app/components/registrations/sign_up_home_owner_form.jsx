let authenticationListener = null;

let signUpHomeOwnerFields = {
  'email':             { label: 'Email', formName: 'user[email]' },
  'password':          { label: 'Mật khẩu', formName: 'user[password]' },
  'user_profile':      { label: 'Thông tin người dùng', formName: 'user[user_profile_attributes]' },
  'user_profile.name': { label: 'Họ và Tên', formName: 'user[user_profile_attributes[name]]' }
};

class SignUpHomeOwnerForm extends React.Component {
  constructor(props) {
    super(props);

    let currentState = AuthenticationStore.getState();

    this.state = {
      signUpFormData: {
        home_owner: currentState.signUpFormData['home_owner']
      },
      signUpErrors: currentState.signUpErrors,
      isSendingRequest: currentState.isSendingRequest,
      fullErrorMessages: [],
      currentUser: currentState.currentUser
    };

    this.componentDidMount      = this.componentDidMount.bind(this);
    this.componentWillUnmount   = this.componentWillUnmount.bind(this);
    this.handleFieldChange      = this.handleFieldChange.bind(this);
    this.handleFormSubmit       = this.handleFormSubmit.bind(this);
    this.onAuthenticationChange = this.onAuthenticationChange.bind(this);
  }

  componentDidMount() {
    authenticationListener = AuthenticationStore.addListener(this.onAuthenticationChange);
  }

  componentWillUnmount() {
    if (authenticationListener) authenticationListener.remove();
  }

  handleFieldChange(e) {
    let { name, value } = e.target;

    let { signUpFormData, signUpErrors } = this.state;

    switch (name) {
      case 'user[user_profile_attributes]':
      case 'user[user_profile_attributes[name]]':
        signUpFormData['home_owner'].user.user_profile_attributes.name = value;
        if (signUpErrors) {
          delete signUpErrors['user_profile'];
          delete signUpErrors['user_profile.name'];
        }
        break;
      case 'user[email]':
        signUpFormData['home_owner'].user.email = value;
        if (signUpErrors) {
          delete signUpErrors['email'];
        }
        break;
      case 'user[password]':
        signUpFormData['home_owner'].user.password = value;
        if (signUpErrors) {
          delete signUpErrors['password'];
        }
        break;
    }

    let fullErrorMessages = APIClient.buildFullMessages(signUpErrors, signUpHomeOwnerFields);

    this.setState({ signUpFormData, signUpErrors, fullErrorMessages });
  }

  handleFormSubmit(e) {
    e.preventDefault();

    let { signUpFormData } = this.state;

    AuthenticationActions.signUp(signUpFormData, 'home_owner');
  }

  onAuthenticationChange() {
    let newState = AuthenticationStore.getState();

    let signUpErrors = newState.signUpErrors;

    let fullErrorMessages = APIClient.buildFullMessages(signUpErrors, signUpHomeOwnerFields);

    // sign up successfully
    if (!signUpErrors && newState.currentUser) {
      Authentication.storeUser(newState.currentUser);
      location.href = '/';
    }

    this.setState({
      signUpFormData: {
        home_owner: newState.signUpFormData['home_owner']
      },
      signUpErrors,
      isSendingRequest: newState.isSendingRequest,
      fullErrorMessages,
      currentUser: newState.currentUser
    });
  }

  render() {
    let { images } = this.props;

    let { signUpFormData, signUpErrors, isSendingRequest, fullErrorMessages } = this.state;

    let formClass = classNames('ui', 'large', 'form', { error: !!signUpErrors });

    let nameFieldClass = classNames('field', {
      error: !!(signUpErrors && (signUpErrors['user_profile']  || signUpErrors['user_profile.name']))
    });
    let emailFieldClass    = classNames('field', { error: !!(signUpErrors && signUpErrors['email']) });
    let passwordFieldClass = classNames('field', { error: !!(signUpErrors && signUpErrors['password']) });

    let submitButtonClass = classNames('ui', 'fluid', 'primary', 'big', 'button', { loading: isSendingRequest });

    return (
      <div className="six wide computer sixteen wide mobile column">
        <div className="ui padded sign-up-box segment">
          <h3><img src={images.logo} /></h3>
          <form className={formClass} method="POST" onSubmit={this.handleFormSubmit}>
            <ErrorMessagesBox errorMessages={fullErrorMessages} />

            <div className={nameFieldClass}>
              <input type="text" autoFocus="true"
                     name={signUpHomeOwnerFields['user_profile.name'].formName}
                     placeholder={signUpHomeOwnerFields['user_profile.name'].label}
                     value={signUpFormData['home_owner'].user.user_profile_attributes.name}
                     onChange={this.handleFieldChange} />
            </div>

            <div className={emailFieldClass}>
              <input type="email"
                     name={signUpHomeOwnerFields['email'].formName}
                     placeholder={signUpHomeOwnerFields['email'].label}
                     value={signUpFormData['home_owner'].user.email}
                     onChange={this.handleFieldChange} />
            </div>

            <div className={passwordFieldClass}>
              <input type="password"
                     name={signUpHomeOwnerFields['password'].formName}
                     placeholder={signUpHomeOwnerFields['password'].label}
                     value={signUpFormData['home_owner'].user.password}
                     onChange={this.handleFieldChange} />
            </div>

            <button className={submitButtonClass} type="submit">ĐĂNG KÍ TÀI KHOẢN</button>
            <br />
            <p className="small muted text">
              Khi đăng ký là bạn đồng ý với <a href="/">Điều khoản sử dụng</a> và <a href="/">Quy chế hoạt động</a>.
            </p>
          </form>
        </div>
      </div>
    );
  }
}

SignUpHomeOwnerForm.propTypes = {
  requestParams: React.PropTypes.object,
  images:        React.PropTypes.object
};
