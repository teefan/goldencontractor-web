class SignUpView extends React.Component {
  constructor(props) {
    super(props);

    let { requestParams } = props;

    this.state = {
      accountType: requestParams.account_type
    };

    this.renderForm = this.renderForm.bind(this);
  }

  renderForm() {
    let { accountType } = this.state;

    switch (accountType) {
      case 'chu_nha':
        return <SignUpHomeOwnerForm {...this.props} />;
      case 'nha_thau':
        return <SignUpContractorForm {...this.props} />;
      default:
        return <SignUpTypeSelect {...this.props} />;
    }
  }

  render() {
    return (
      <div className="ui middle aligned center aligned grid authentication-box container">
        {this.renderForm()}
      </div>
    );
  }
}

SignUpView.propTypes = {
  requestParams: React.PropTypes.object,
  images:        React.PropTypes.object
};
