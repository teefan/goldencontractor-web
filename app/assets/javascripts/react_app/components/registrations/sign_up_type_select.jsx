class SignUpTypeSelect extends React.Component {
  render() {
    let { images } = this.props;

    return (
      <div className="six wide computer sixteen wide mobile column">
        <div className="ui padded sign-up-box segment">
          <h1><img src={images.logo} /></h1>
          <p><a href="/dang_ky/chu_nha" className="fluid ui big yellow button">BẠN LÀ CHỦ NHÀ</a></p>
          <p><a href="/dang_ky/nha_thau" className="fluid ui big basic yellow button">BẠN LÀ NHÀ THẦU</a></p>
          <br />
          <p>Bạn đã có sẵn tài khoản? <a href="/dang_nhap">Đăng nhập ngay.</a></p>
        </div>
      </div>
    );
  }
}

SignUpTypeSelect.propTypes = {
  images: React.PropTypes.object
};
