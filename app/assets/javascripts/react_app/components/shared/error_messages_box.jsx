class ErrorMessagesBox extends React.Component {
  constructor(props) {
    super(props);

    this.renderMessages = this.renderMessages.bind(this);
  }

  renderMessages(errorMessages) {
    if (!errorMessages || errorMessages.length < 1) return null;

    let errorsContent = null;
    let errorLines = [];
    for (let i = 0; i < errorMessages.length; i++) {
      let errorLine = <div className="item" key={`error-${i}`}>{errorMessages[i]}</div>;
      errorLines.push(errorLine);
    }

    errorsContent =
      <div className="ui text left error small message">
        <div className="header">Có {errorMessages.length} lỗi:</div>
        <div className="ui bulleted list">
          {errorLines}
        </div>
      </div>;

    return errorsContent;
  }

  render() {
    let { errorMessages } = this.props;

    return this.renderMessages(errorMessages);
  }
}

ErrorMessagesBox.defaultProps = {
  errorMessages: []
};

ErrorMessagesBox.propTypes = {
  errorMessages: React.PropTypes.array
};
