const Authentication = {
  storeUser(userData) {
    let storeData = {
      id:         userData.id,
      name:       userData.user_profile.name,
      auth_token: userData.auth_token
    };

    let options = {
      domain: `.${APP_HOST}`
    };

    // if remember, store cookie for 7 days
    if (userData.remember_me) {
      options['expires'] = 7;
    }

    Cookies.set('current_user', storeData, options);
  },

  clearUser() {
    let options = {
      domain: `.${APP_HOST}`
    };

    Cookies.remove('current_user', options);
  }
};
