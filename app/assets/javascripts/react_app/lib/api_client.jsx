const APIClient = {
  headers() {
    return {
      'Accept': 'application/json; version=1'
    };
  },

  get(resource, options) {
    return this.sendRequest(resource, 'GET', options);
  },

  post(resource, options) {
    return this.sendRequest(resource, 'POST', options);
  },

  put(resource, options) {
    return this.sendRequest(resource, 'PUT', options);
  },

  patch(resource, options) {
    return this.sendRequest(resource, 'PATCH', options);
  },

  delete(resource, options) {
    return this.sendRequest(resource, 'DELETE', options);
  },

  sendRequest(path, method, options) {
    let _this = this;

    let data, beforeSend, done, fail;
    if (options) {
      data       = options['data'];
      beforeSend = options['beforeSend'];
      done       = options['done'];
      fail       = options['fail'];
    }

    let $requestXHR = $.ajax({
      url: `${API_URL}/${path}`,
      method,
      dataType: 'json',
      contentType: 'application/json',
      crossDomain: true,
      headers: _this.headers(),
      beforeSend: beforeSend,
      data: method !== 'GET' ? JSON.stringify(data) : data
    });

    if (done) {
      $requestXHR = $requestXHR.done(done);
    }

    if (fail) {
      $requestXHR = $requestXHR.fail(fail);
    }

    return $requestXHR;
  },

  buildFullMessagesObject(errors, fields) {
    if (!errors) return {};

    let hasError = false;
    let errorMessages = {};
    for (let key in errors) {
      hasError = true;
      if (fields && fields[key]) {
        errorMessages[key] = `${fields[key].label} ${errors[key].join(', ')}`;
      } else {
        errorMessages[key] = typeof errors[key].constructor === Array ? errors[key].join(', ') : errors[key];
      }
    }

    return hasError ? errorMessages : {};
  },

  buildFullMessages(errors, fields) {
    if (!errors) return [];

    let hasError = false;
    let errorMessages = [];
    for (let key in errors) {
      hasError = true;
      if (fields && fields[key]) {
        errorMessages.push(`${fields[key].label} ${errors[key].join(', ')}`);
      } else {
        errorMessages.push(typeof errors[key].constructor === Array ? errors[key].join(', ') : errors[key]);
      }
    }

    return hasError ? errorMessages : [];
  }
};
