const Logger = {
  log() {
    if (typeof APP_ENV === 'string' && APP_ENV !== 'production' && typeof console === 'object' && console) {
      console.log.apply(console, arguments); // eslint-disable-line
    } else {
      return false;
    }
  }
};
