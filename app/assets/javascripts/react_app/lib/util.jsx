const Util = {
  toFixed(value, precision) {
    let power = Math.pow(10, precision || 0);
    return Math.round(value * power) / power;
  }
};
