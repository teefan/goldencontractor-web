const JobActions = {
  sendJobRequest(jobFormData) {
    AppDispatcher.dispatch({
      type: 'SEND_JOB_REQUEST',
      jobFormData
    });
  },

  validateJobRequest(jobFormData) {
    AppDispatcher.dispatch({
      type: 'VALIDATE_JOB_REQUEST',
      jobFormData
    });
  }
};
