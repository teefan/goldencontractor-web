const ServiceAndTypeActions = {
  loadConstructionServices() {
    AppDispatcher.dispatch({
      type: 'LOAD_CONSTRUCTION_SERVICES'
    });
  },

  loadConstructionTypes() {
    AppDispatcher.dispatch({
      type: 'LOAD_CONSTRUCTION_TYPES'
    });
  }
};
