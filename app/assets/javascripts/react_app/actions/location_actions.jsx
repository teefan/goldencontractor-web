const LocationActions = {
  loadCountries() {
    AppDispatcher.dispatch({
      type: 'LOAD_COUNTRIES'
    });
  },

  loadCities(countryID) {
    AppDispatcher.dispatch({
      type: 'LOAD_CITIES',
      countryID
    });
  },

  loadDistricts(cityID) {
    AppDispatcher.dispatch({
      type: 'LOAD_DISTRICTS',
      cityID
    });
  }
};
