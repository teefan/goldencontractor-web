const AuthenticationActions = {
  signUp(signUpFormData, accountType) {
    AppDispatcher.dispatch({
      type: 'SIGN_UP',
      signUpFormData,
      accountType
    });
  },

  signIn(signInFormData) {
    AppDispatcher.dispatch({
      type: 'SIGN_IN',
      signInFormData
    });
  }
};
