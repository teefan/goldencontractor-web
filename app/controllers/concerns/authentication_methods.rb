# frozen_string_literal: true
module AuthenticationMethods
  extend ActiveSupport::Concern

  included do
    attr_accessor :current_user
    before_action :init_user
  end

  private

  def init_user
    @current_user = JSON.parse(cookies['current_user']) if cookies['current_user']
  end

  def user_signed_in?
    @current_user ? true : false
  end
end
