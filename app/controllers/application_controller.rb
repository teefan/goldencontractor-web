# frozen_string_literal: true
class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  respond_to :html

  before_action :set_locale

  include AuthenticationMethods

  before_action :init_variables

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  # def default_url_options
  #   { locale: I18n.locale }
  # end

  private

  # def extract_locale_from_accept_language_header
  #   request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
  # end

  def init_variables
    # p current_user
  end
end
