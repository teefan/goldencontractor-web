# frozen_string_literal: true
module AuthenticationHelper
  def current_user
    @current_user
  end

  def user_signed_in?
    @current_user ? true : false
  end
end
