# frozen_string_literal: true
module MenuHelper
  def home_menu_class
    current_page?(home_index_path) || current_page?(root_path) ? 'item active' : 'item'
  end

  def jobs_menu_class
    current_page?(jobs_path) ? 'item active' : 'item'
  end
end
