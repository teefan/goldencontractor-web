# frozen_string_literal: true
module ReactHelper
  def view_params
    {
      requestParams: params,
      images: {
        logo: image_path('logo.png')
      }
    }
  end
end
